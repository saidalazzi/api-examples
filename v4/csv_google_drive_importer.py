#!/usr/bin/env python

import csv
import os
import tempfile

import googleapiclient
from googleapiclient import discovery
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
import requests


GOOGLE_API_SCOPES = ['https://www.googleapis.com/auth/drive']
MAYAN_API_DOCUMENT_FILE_ACTION = 1

# Replace these:
CSV_FILE_PATH = 'example_file.csv'
MAYAN_API_TOKEN = '1234...'
MAYAN_DOCUMENT_TYPE_LABEL = 'Example document type label'
MAYAN_URL = 'http://example.com:8000/'


class MayanCSVImporter:
    """
    Scans a CSV file containing Google Drive file ID numbers.
    The Google Drive files are uploaded via the API and assigned the metadata
    values from the CSV.
    """

    _document_type_cache = {}
    _metadata_type_cache = {}

    def __init__(self, api_token, csv_file_path, document_file_action, document_type_label, url):
        headers = {'Authorization': 'Token {}'.format(api_token)}

        self.csv_file_path = csv_file_path
        self.document_type_label = document_type_label
        self.document_file_action = document_file_action
        self.session = requests.Session()
        self.session.headers.update(headers)
        self.mayan_url = url
        self.mayan_api_url = '{}api/v4/'.format(self.mayan_url)
        self.mayan_api_document_list_url = '{}documents/'.format(self.mayan_api_url)
        self.mayan_api_document_metadata_list_url_template = '{}documents/{{}}/metadata/'.format(self.mayan_api_url)
        self.mayan_api_document_file_list_url_template = '{}documents/{{}}/files/'.format(self.mayan_api_url)
        self.mayan_api_document_type_list_url = '{}document_types/'.format(self.mayan_api_url)
        self.mayan_api_document_type_metadata_type_list_url_template = '{}document_types/{{}}/metadata_types/'.format(self.mayan_api_url)
        self.mayan_api_metadata_type_list_url = '{}metadata_types/'.format(self.mayan_api_url)
        self.mayan_api_search_document_type_url_template = '{}search/documents.DocumentType/?label={{}}'.format(self.mayan_api_url)
        self.mayan_api_search_metadata_type_url_template = '{}search/metadata.MetadataType/?name={{}}'.format(self.mayan_api_url)

    def get_or_create_document_type_id(self, label):
        """
        Search or create, and cache the document type ID.
        """
        if label not in self.__class__._document_type_cache:
            url = self.mayan_api_search_document_type_url_template.format(label)
            response = self.session.get(url=url)

            if response.json()['count'] == 0:
                print('Creating document type: "{}"'.format(label))

                response = self.session.post(
                    url=self.mayan_api_document_type_list_url,
                    data={
                        'label': label
                    }
                )
                response.raise_for_status()

                self.__class__._document_type_cache[label] = response.json()['id']
            else:
                self.__class__._document_type_cache[label] = response.json()['results'][0]['id']

        return self.__class__._document_type_cache[label]

    def get_or_create_metadata_type_id(self, name, document_type_id, label=None):
        """
        Search or create, and cache the metadata type ID.
        If the metadata type is created, it will be associated to the
        document type id.
        """
        if name not in self.__class__._metadata_type_cache:
            url = self.mayan_api_search_metadata_type_url_template.format(name)
            response = self.session.get(url=url)

            if response.json()['count'] == 0:
                print('Creating metadata type: {} "{}"'.format(name, label))

                response = self.session.post(
                    url=self.mayan_api_metadata_type_list_url,
                    data={
                        'name': name, 'label': label or name
                    }
                )
                response.raise_for_status()

                metadata_type_id = response.json()['id']

                url = self.mayan_api_document_type_metadata_type_list_url_template.format(document_type_id)
                response = self.session.post(
                    url=url, data={
                        'metadata_type_id': metadata_type_id
                    }
                )

                self.__class__._metadata_type_cache[name] = metadata_type_id
            else:
                self.__class__._metadata_type_cache[name] = response.json()['results'][0]['id']

        return self.__class__._metadata_type_cache[name]

    def create_document(self, document_type_id, label):
        response = self.session.post(
            url=self.mayan_api_document_list_url, data={
                'document_type_id': document_type_id,
                'label': label
            }
        )
        response.raise_for_status()

        return response.json()['id']

    def set_document_metadata(self, document_id, metadata_type_id, value):
        url = self.mayan_api_document_metadata_list_url_template.format(
            document_id
        )

        response = self.session.post(
            url=url, data={
                'metadata_type_id': metadata_type_id,
                'value': value
            }
        )
        response.raise_for_status()

        return response.json()['id']

    def upload_document_file(self, action, document_id, file_object, filename, comment=None):
        url = self.mayan_api_document_file_list_url_template.format(document_id)

        response = self.session.post(
            url=url, files={
                'file_new': file_object
            }, data={
                'action': action,
                'comment': comment,
                'filename': filename
            }
        )
        response.raise_for_status()

    def google_drive_download_file(self, file_object, file_id):
        """
        Download a Google Drive file to the provided file_object.
        Validates the credentials on every call to ensure the token is not
        expired.
        """
        credentials = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.json'):
            credentials = Credentials.from_authorized_user_file(
                'token.json', GOOGLE_API_SCOPES
            )

        # If there are no (valid) credentials available, let the user log in.
        if not credentials or not credentials.valid:
            if credentials and credentials.expired and credentials.refresh_token:
                credentials.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', GOOGLE_API_SCOPES
                )
                credentials = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.json', 'w') as token:
                token.write(credentials.to_json())

        service = discovery.build('drive', 'v3', credentials=credentials)

        request = service.files().get_media(fileId=file_id)
        media_request = googleapiclient.http.MediaIoBaseDownload(file_object, request)

        while True:
            status, done = media_request.next_chunk()
            print(' - Download %d%%.' % int(status.progress() * 100))

            if done:
                print (' - Download complete.')
                break

    def run(self):
        with open(file=self.csv_file_path, mode='r') as file_object:
            # Create document type if it doesn't exist.
            document_type_id = self.get_or_create_document_type_id(
                label=self.document_type_label
            )

            # Create metadata types if they don't exist.
            metadata_type_1_id = self.get_or_create_metadata_type_id(
                document_type_id=document_type_id, name='metadata_type_1',
                label='Metadata type 1'
            )
            metadata_type_2_id = self.get_or_create_metadata_type_id(
                document_type_id=document_type_id, name='metadata_type_2',
                label='Metadata type 2'
            )
            line_number = 1

            # Skip header.
            file_object.readline()

            for row in csv.reader(file_object):
                line_number += 1
                row_filename, row_google_drive_file_id, row_metadata_1_value, row_metadata_2_value = row

                print('Importing file: ({}) {}'.format(line_number, row_filename))

                document_id = self.create_document(
                    document_type_id=document_type_id, label=row_filename
                )

                self.set_document_metadata(
                    document_id=document_id,
                    metadata_type_id=metadata_type_1_id,
                    value=row_metadata_1_value
                )

                self.set_document_metadata(
                    document_id=document_id,
                    metadata_type_id=metadata_type_2_id,
                    value=row_metadata_2_value
                )

                with tempfile.TemporaryFile() as file_object:
                    self.google_drive_download_file(
                        file_object=file_object,
                        file_id=row_google_drive_file_id
                    )
                    file_object.seek(0)

                    self.upload_document_file(
                        action=self.document_file_action,
                        document_id=document_id, file_object=file_object,
                        filename=row_filename
                    )

                print(' - Upload complete.')
                if line_number >= 1000:
                    exit(1)


if __name__ == '__main__':
    csv_importer = MayanCSVImporter(
        api_token=MAYAN_API_TOKEN,
        csv_file_path=CSV_FILE_PATH,
        document_file_action=MAYAN_API_DOCUMENT_FILE_ACTION,
        document_type_label=MAYAN_DOCUMENT_TYPE_LABEL,
        url=MAYAN_URL,
    )
    csv_importer.run()
