#!/usr/bin/env python

from pathlib import Path

import requests

DOWNLOAD_CHUNK_SIZE = 8192
VERIFY_SSL = False

# Note: Adjust these for your installation.
# API token to use for access.
MAYAN_API_TOKEN = '1234...'
# Document type label of the documents to fetch.
MAYAN_DOCUMENT_TYPE_LABEL = 'Example document type'
# Base URL of the Mayan installation. Include trailing slash (/).
MAYAN_URL = 'https://example.com:8000/'


class DocumentPageExporter:
    def __init__(self, api_token, document_type_label, url, verify_ssl):
        headers = {'Authorization': 'Token {}'.format(api_token)}

        self.document_type_label = document_type_label
        self.session = requests.Session()
        self.session.headers.update(headers)
        self.mayan_url = url
        self.mayan_api_url = '{}api/v4/'.format(self.mayan_url)
        self.mayan_api_document_list_url = '{}documents/'.format(self.mayan_api_url)
        self.mayan_api_document_version_page_list_url_template = '{}documents/{{}}/versions/{{}}/pages/'.format(self.mayan_api_url)
        self.mayan_api_search_document_type_url_template = '{}search/documents.DocumentType/?label={{}}'.format(self.mayan_api_url)
        self.verify_ssl = verify_ssl

    def get_document_type_id(self, label):
        url = self.mayan_api_search_document_type_url_template.format(label)
        response = self.session.get(url=url, verify=self.verify_ssl)
        response.raise_for_status()

        return response.json()['results'][0]['id']

    def paged_response_iterator(self, response):
        # Since API responses include only a limited number of results,
        # this iterator will perform next page requests automatically,
        # and give the impression of a continuous flow of API results.
        while True:
            for item in response.json()['results']:
                yield item

            if response.json()['next']:
                response = self.session.get(url=response.json()['next'], verify=self.verify_ssl)
                response.raise_for_status()
            else:
                break

    def run(self):
        path_base = Path('.')

        # Create a top level folder to hold all images.
        # Structure is:
        # - documents/                 <- Static
        #   - #                        <- ID of the document
        #     - #                      <- ID of the document version
        #       - #.jpg                <- ID of the document version page
        path_documents = path_base / Path('documents')
        path_documents.mkdir(exist_ok=True)

        document_type_id = self.get_document_type_id(
            label=self.document_type_label
        )

        document_list_response = self.session.get(
            url=self.mayan_api_document_list_url, verify=self.verify_ssl
        )
        document_list_response.raise_for_status()

        for document in self.paged_response_iterator(response=document_list_response):
            # Only download the document of the type we are interested in.
            if document['document_type']['id'] == document_type_id:
                print(
                    'Processing document: ({}) {}'.format(
                        document['id'], document['label']
                    )
                )

                # Create a folder for the document using the ID.
                path_document = path_documents / Path(str(document['id']))
                path_document.mkdir(exist_ok=True)

                # Get the page list URL of the active version for the
                # document we are processing.
                document_version_page_list_response = self.session.get(
                    url=document['version_active']['page_list_url'],
                    verify=self.verify_ssl
                )

                # Create a folder for the document version inside the folder
                # of the document.
                path_document_version = path_document / Path(
                    str(document['version_active']['id'])
                )
                path_document_version.mkdir(exist_ok=True)

                # Iterate over the document version pages.
                for document_version_page in self.paged_response_iterator(response=document_version_page_list_response):
                    # Build the file path where the image will be saved.
                    path_document_version_page_image = path_document_version / Path(
                        '{}.jpg'.format(document_version_page['id'])
                    )

                    # Fetch the image data.
                    document_version_page_image_response = self.session.get(
                        url=document_version_page['image_url'], stream=True,
                        verify=self.verify_ssl
                    )
                    document_version_page_image_response.raise_for_status()

                    # Create a new file for the page and copy the page
                    # image data.
                    with path_document_version_page_image.open(mode='wb') as file_object:
                        for chunk in document_version_page_image_response.iter_content(chunk_size=DOWNLOAD_CHUNK_SIZE):
                            file_object.write(chunk)


if __name__ == '__main__':
    page_exporter = DocumentPageExporter(
        api_token=MAYAN_API_TOKEN,
        document_type_label=MAYAN_DOCUMENT_TYPE_LABEL,
        url=MAYAN_URL,
        verify_ssl=VERIFY_SSL
    )
    page_exporter.run()
